<?php

use App\Events\MyEvent;
use App\Http\Controllers\Web\User\DashboardController;
use App\Http\Controllers\Web\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/send', function () {
    return view('send');
});

Route::get('/send-event', function () {
    event(new MyEvent('hello world'));
});

//Authentication
Route::get('/signup', [AuthController::class, 'signup'])->name('signup');
Route::post('/signup-process', [AuthController::class, 'signupProcess'])->name('signupProcess');
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login-process', [AuthController::class, 'loginProcess'])->name('loginProcess');

Route::middleware('auth')->group(function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

    //Dashboard
    Route::prefix('user/dashboard')->name('user.dashboard')->group(function () {
        Route::get('/', [DashboardController::class, 'dashboard'])->name('');
        Route::get('/content', [DashboardController::class, 'content'])->name('.content');
    });
});

