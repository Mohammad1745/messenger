<?php


namespace App\Http\Services\User;

use App\Http\Services\Base\UserService;
use App\Http\Services\ResponseService;
use Exception;

class DashboardService extends ResponseService
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * DashboardService constructor.
     * @param UserService $userService
     */
    public function __construct (UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @return array
     */
    public function content (): array
    {
        try {
            return $this->response()->success();
        } catch (Exception $exception) {
            return $this->response()->error($exception->getMessage());
        }
    }
}
