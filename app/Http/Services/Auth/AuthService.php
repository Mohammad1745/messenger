<?php


namespace App\Http\Services\Auth;

use App\Http\Services\Base\UserService;
use App\Http\Services\ResponseService;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AuthService extends ResponseService
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * AuthService constructor.
     * @param UserService $userService
     */
    public function __construct (UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param object $request
     * @return array
     */
    public function signupProcess (object $request): array
    {
        try {
            DB::beginTransaction();
            $user = $this->userService->create( $this->userService->userDataFormatter( $request->all()));
            DB::commit();

            return $this->response()->success(__("Successfully signed up."));
        } catch (Exception $exception) {
            DB::rollBack();

            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @param object $request
     * @return array
     */
    public function loginProcess (object $request): array
    {
        try {
            if (Auth::attempt( $this->_credentials( $request->only('phone', 'password')))){
                return $this->response()->success('Logged In Successfully.');
            } else {
                return $this->response()->error('Wrong Email Or Password');
            }
        } catch (Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @param string $requestType
     * @return array
     */
    public function logoutProcess(string $requestType='api'): array
    {
        try {
            if(Auth::user()){
                $requestType=='api' ?
                    Auth::user()->token()->revoke() :
                    Auth::logout();

                return $this->response()->success('Logged Out Successfully');
            } else {
                return $this->response()->error('Already Logged Out.');
            }
        } catch (Exception $exception) {
            return $this->response()->error( $exception->getMessage());
        }
    }

    /**
     * @param array $data
     * @return array
     */
    private function _credentials (array $data) : array {
        if (isPhone($data['phone'])){
            return [
                'phone' => $data['phone'],
                'password' => $data['password']
            ];
        }
        return filter_var( $data['phone'], FILTER_VALIDATE_EMAIL) ? [
            'email' => $data['phone'],
            'password' => $data['password']
        ] : [
            'username' => $data['phone'],
            'password' => $data['password']
        ];
    }
}
