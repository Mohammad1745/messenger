<?php

namespace App\Http\Requests\Web;

use App\Http\Requests\Request;

class SignupRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ():array
    {
        return [
            'name' => 'required|string',
            'email' => 'required|unique:users',
            'phone' => 'required|unique:users|regex: /^(01){1}[1-9]{1}[0-9]{8}$/'
        ];
    }
}
